# Azul

Implementación en C de juego de mesa Azul[^1].

[[_TOC_]]

## Datos Jugador: `utils.h`

### Constante simbólica: `CANT_JUGS`

Cantidad de jugadores (entre `2` y `4`).

### Enumeración: `Color`

- `VACIO`: igual a `0`.
- `AZUL`
- `ROJO`
- `MORADO`
- `NEGRO`
- `BLANCO`


## Datos Jugador: `jugador.h`

### Estructura: `Patrones`

- `colorAzulejo`: arreglo de tamaño `5` con enumeraciones de tipo `Color`
- `cantidad`: arreglo de tamaño `5` con cantidad (entre 0 y posición de
  elemento mas `1`).

### Constante Global: `MOSAICO`

Matriz de tamaño `5` por `5` con enumeraciones de tipo `Color`. Los valores son
contantes:


| F\C | `0`     | `1`     | `2`     | `3`     | `4`     |
| :-: | :---    | :---    | :---    | :---    | :---    |
| `0` | AZUL    | ROJO    | MORADO  | NEGRO   | BLANCO  |
| `1` | BLANCO  | AZUL    | ROJO    | MORADO  | NEGRO   |
| `2` | NEGRO   | BLANCO  | AZUL    | ROJO    | MORADO  |
| `3` | MORADO  | NEGRO   | BLANCO  | AZUL    | ROJO    |
| `4` | ROJO    | MORADO  | NEGRO   | BLANCO  | AZUL    |

### Estructura: `Jugador`

- `nombre`: arreglo de caracteres de tamaño `20`.
- `puntos`: contador entero de puntaje (entre `0` y `100`).
- `lineas`: estructura de tipo `Patrones`.
- `pared`: matriz con enumeraciones de tipo `Color` de tamaño `5` por `5`.
- `piso`: contador de penalizaciones (entre `0` y `7`).
- `primerTurno`: `1` si el jugador es el primero en tomar azulejos del centro
  de mesa en una ronda, de lo contrario `0`.

## Datos Proveedor: `proveedor.h`

### Constante simbólica: `CANT_MOST`

Cantidad de mostradores: `((CANT_JUGS * 2) + 1)`.

### Constante simbólica: `FIL_AZU`

Cantidad de filas de miembro `azulejos` en `Proveedor`: `(CANT_MOST + 3)`.

### Estructura: `Proveedor`

- `azulejos`: matriz de tamaño `FIL_AZU` por `5`, representando
  por fila:

   - `0`: bolsa.
   - `1`: basura.
   - `2`: centro de mesa.
   - `3`: mostrador 1.
   - ...
   - `FIL_AZU`: mostrador `CANT_MOST`.

   Cada columna representa:

   - `0`: Azul.
   - `1`: Rojo.
   - `2`: Morado.
   - `3`: Negro.
   - `4`: Blanco.

- `marcadorTurno`: `1` si marcador de primer turno se encuentra en el centro
  de mesa, de lo contrario `0`.

## Control de flujo: `azul.c`

### Función: `main`

1. Declaro estructura `partida` de tipo `Proveedor`.
1. Declaro arreglo `jugadores` de tipo `Jugador` y tamaño `CANT_JUGS`.
1. Inicializo generador de de números aleatorios.
1. Inicializo `partida` invocando `iniciarPartida()`.
1. Inicializo jugadores invocando `iniciarJugadores()`.
1. Declaro entero positivo `jugadorInicial`.
1. Hacer:
   1. Invoco `surtirMostradores()` para asignar `4` azulejos aleatorios a cada
      mostrador.
   1. Ubico marcador en centro de mesa asignando verdadero a miembro
      `marcadorTurno` de estructura `partida`.
   1. Invoco `escogerIniciadorRonda()` y asignar valor retornado a
      `jugadorInicial`.
   1. Invoco `jugarRonda()`.
   1. Mientras retorno de `calcularPuntaje()` es igual a `0`.
1. Invoco `calcularPuntajeFinal()` para calcular puntaje final de jugadores.
1. Invoco `imprimirResultados()` para visualizar jugadores ordenados por
   puntaje descendente.

### Función: `jugarRonda`

Lanza turnos a partir de jugador iniciador de ronda hasta que el centro de
mesa y mostradores quedan vacíos.

#### Entradas

- `jugs`: arreglo de tipo `Jugador` con jugadores de tamaño `CANT_JUGS`.
- `partPtr`: estructura de tipo `Proveedor` con distribución central de azulejos
  (referencia).
- `iJug`: posición en arreglo `jugs` de jugador iniciador de ronda.

#### Salida

- Ninguna.

#### Pseudo-código

1. Imprimo: "Inicio de ronda..."
1. Mientras retorno de `proveedorVacio()` sea falso:
   1. Declaro variable de `azCol` tipo enumeración `Color` y asigno `VACIO`.
   1. Declaro entero positivo `azCan` para cantidad de azulejos y asigno `0`.
   1. Declaro entero positivo `azDes` para cantidad de azulejos descartados y
      asigno `0`.
   1. Imprimo: "Turno de: `jugs[iJug].nombre`".
   1. Invoco `imprimirTablero()` para jugador en posición `iJug` de arreglo
      `jugs`.
   1. Si retorno de `escogerAzulejos()` es verdadero:
      1. Asigno verdadero a miembro `primerTurno` de jugador en posición
         `iJug`.
      1. Invoco `agregarPenalizacion()` para miembro `piso` de jugador en
         posición `iJug`.
   1. Invoco `imprimirTablero()` para jugador en posición `iJug` de arreglo
      `jugs`.
   1. Asigno a `azDes` retorno de `ubicarAzulejos()` con azulejo(s) de color
      `azCol` y cantidad `azCan` para el jugador en posición `iJug`.
   1. Si `azDes` es mayor a `0`:
      1. Sumo al elemento en fila `1` (basura) y columna `azCol - 1` el valor
         de `azDes`.
   1. Asigno a `iJug` el módulo `CANT_JUGS` de `iJug` incrementado en `1`.
1. Imprimo: "Fin de ronda..."

## Acciones sobre jugador: `jugador.c`

### Función: `iniciarJugadores`

Obtiene nombre de jugador, inicializa tablero vacío y puntaje en  `0`.

#### Entradas

- `jugs`: arreglo de tipo `Jugador` con jugadores.

#### Salida

- Ninguna.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `CANT_JUGS`; incremento `i` en `1`:
   1. Pregunto a usuario por nombre de jugador `i + 1`.
   1. Obtengo de usuario nombre de jugador `i + 1` y asigno a miembro `nombre`
      de jugador en posición `i` de arreglo `jugs`.
   1. Asigno `0` a miembro `puntos` de jugador en posición `i` de arreglo
      `jugs`.
   1. Para `j` desde `0`; hasta `j` menor a `5`; incremento `j` en `1`:
      1. Asigno `VACIO` a elemento en posición `j` de miembro `lineas.colorAzulejo`
         de jugador en posición `i` de arreglo `jugs`.
      1. Asigno `0` a elemento en posición `j` de miembro `lineas.cantidad` de
         jugador en posición `i` de arreglo `jugs`.
   1. Para `j` desde `0`; hasta `j` menor a `25`; incremento `j` en `1`:
      1. Asigno `VACIO` a elemento en fila `j / 5` y columna `j % 5` de miembro
         `pared` de jugador en posición `i` de arreglo `jugs`.
   1. Asigno `0` a miembro `piso` de jugador en posición `i` de arreglo
      `jugs`.
   1. Asigno `false` a miembro `primerTurno` de jugador en posición `i` de arreglo
      `jugs`.

### Función: `escogerIniciadorRonda`

Busca marcador de turno entre jugadores, retorna posición de jugador con
marcador y asigna falso a marcador de jugador. Si no se encuentra marcador
entre jugadores, se retorna una posición aleatoria.

#### Entradas

- `jugs`: arreglo de tipo `Jugador` con jugadores de tamaño `CANT_JUGS`.

#### Salida

Posición del jugador que debe iniciar la ronda.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `CANT_JUGS`; incremento `i` en `1`:
   1. Si miembro `primerTurno` de jugador en posición `i` de arreglo `jugs` es
      igual a `1` (verdadero):
      1. Asigno falso a miembro `primerTurno` de jugador en posición `i` de
         arreglo `jugs`.
      1. Retorno `i`.
1. Retorno valor aleatorio entre `0` y `CANT_JUGS - 1`.

### Función: `imprimirTablero`

Muestra en pantalla contenidos del tablero del jugador (estructura) recibido
como parámetro.

#### Entradas

- `jug`: estructura constante de tipo `Jugador` con datos del jugador.

#### Salida

Ninguna (impresión en pantalla).

#### Pseudo-código

1. Imprimo: "Puntaje: `jug.puntos`".
1. Imprimo cambio de línea.
1. Para `i` desde `0`; `i` menor a `5`; incremento `i` en `1`:
   1. Invoco `impFilaPatron()` para fila `i` con color
      `jug.lineas.colorAzulejo` y cantidad `jug.lineas.cantidad`.
   1. Imprimo tabulación.
   1. Invoco `impFilaPared()` para arreglo de la fila `i` de miembro `pared`
      estrucutra `jug`.
   1. Imprimo cambio de línea.
1. Imprimo: "Penalizaciones: `jug.piso`".
1. Imprimo: "Marcador primer turno: `jug.primerTurno`".

### Función: `ubicarAzulejos`

Pregunta y obtiene de usuario ubicación de azulejo(s) sobre alguna línea de
patrón válida. Si sobran azulejos, incrementa puntos de penalización. Retorna
la cantidad de azulejos que no fueron ubicados correctamente.

#### Entradas

- `azCol`: enumeración de tipo `Color`.
- `azCan`: cantidad de azulejos.
- `patrPtr`: estructura con líneas de patrones (referencia).
- `pared`: matriz constante de tamaño `5` por `5` con posiciones ocupadas.
- `pisoPtr`: contador de penalizaciones(referencia).

#### Salida

Cantidad de azulejos que no fueron ubicados correctamente.

#### Pseudo-código

1. Declaro entero para posición de línea `lin` y asigno retorno de
   `buscarColor()` para color `azCol` y arreglo `patrPtr.colorAzulejo` (retorna
   `-1` si no se encuentra color).
1. Si `lin` es igual a `-1`:
   1. Asigno a `lin` retorno de `escogerPatron()`para color `azCol`, arreglo
      `patrs.colorAzulejo` y mosaico `pared` (retorna `-2` si no es posible
      escoger patrón).
1. De lo contrario (color ya existe en línea de patrones):
   1. Imprimo: "Color disponible en línea de patrón No. `lin + 1`.".
1. Si `lin` es diferente a `-2`:
   1. Imprimo: "Ubicando azulejos en línea de patrón No. `lin + 1`.".
   1. Asigno color `azCol` a posición `lin` de arreglo `colorAzulejo`.
   1. Si cantidad de azulejos en patrón `patrPtr[lin]->cantidad` más cantidad
      de azulejos a agregar `azCan` es menor o igual a máximo de capacidad de
      patrón `lin + 1`:
      1. Agrego azulejos `azCan` a patrón `patrPtr[lin]->cantidad`.
      1. Asigno `0` a `azCan`.
   1. De lo contrario (desborde de patrón):
      1. Resto casillas vacías `lin + 1 - patrPtr[lin]->cantidad` a `azCan`.
      1. Asigno máxima capacidad de patrón `lin + 1` a patrón
         `patrs[lin].cantidad`.
      1. Imprimo: "Sobran `azCan` azulejos".
      1. Invoco `agregarPenalizacion()` para contador de penalizaciones
         `pisoPtr` y cantidad de azulejos sobrantes `azCan`.
1. De lo contrario (no se encuentra patrón válido):
   1. Imprimo: "Sobran `azCan` azulejos".
   1. Invoco `agregarPenalizacion()` para contador de penalizaciones `pisoPtr` y
      cantidad de azulejos sobrantes `azCan`.
1. Retorno `azCan`.

### Función: `agregarPenalizacion`

Recibe contador como referencia y la cantidad de penalizaciones a agregar.

#### Entradas

- `piso`: entero positivo (entre `0` y `7`) contador de
  penalizaciones(referencia).
- `pena`: entero positivo con cantidad de penalizaciones.

#### Salida

Ninguna (referencia).

#### Pseudo-código

1. Si suma de contador `piso` con nuevas penalizaciones `pena` es mayor a `7`:
   1. Asigno `7` a `piso`.
1. De lo contrario (no hay desborde):
   1. Incremento `pena` a `piso`.

### Función: `calcularPuntaje`

Calcula e incrementa puntaje por jugador al finalizar una ronda. Retorna `0` si
es el final de partida, de lo contrario `1`.

#### Entradas

- `jugs`: arreglo de tipo `Jugador` con jugadores.
- `basu`: arreglo para descartar azulejos (miembro de `partida`).

#### Salida

`0` si es final de partida, de lo contrario `1`.

#### Pseudo-código

1. Declaro booleano `finPartida` y asigno valor `0` (falso).
1. Para `i` desde `0`; hasta `i` menor a `CANT_JUGS`; incremento `i` en `1`:
   1. Para `j` desde `0`; hasta `j` menor a `5`; incremento `j` en `1`:
      1. Si máximo de capacidad de patrón `j + 1` es igual a `cantidad` para
         patrón `j` en jugador `i`:
         1. Declaro `azCol` de tipo `Color` para almacenar color de azulejo y
            asigno valor de patrón `j` de jugador en posición `i`.
         1. Declaro entero positivo `col` para almacenar posición de columna
            retornada por `buscarColor()` con fila `j` de matriz `MOSAICO` y
            retornada por `buscarColor()` con fila `j` de matriz `MOSAICO` y
            color `azCol`.
         1. Asigno color `azCol` a la posición `j``col` en matriz `pared` de
            jugador en posición `i` de arreglo `jugs`.
         1. Declaro entero positivo `adyHor` para contar azulejos adjacentes
            horizontales y asigno `0`.
         1. Declaro entero positivo `adyVer` para contar azulejos adjacentes
            verticales y asigno `0`.
         1. Invoco `contarAdyacentes()` hacia la derecha y sumo retorno a
            `adyHor`.
         1. Invoco `contarAdyacentes()` hacia la izquierda y sumo retorno a
            `adyHor`.
         1. Invoco `contarAdyacentes()` hacia la arriba y sumo retorno a
            `adyVer`.
         1. Invoco `contarAdyacentes()` hacia la abajo y sumo retorno a
            `adyVer`.
         1. Si `adHor` es mayor a `0`:
            1. Incremento `adyHor + 1` al miembro `puntos` del jugador en
               posición `i` de arreglo `jugs`.
         1. Si `adVer` es mayor a `0`:
            1. Incremento `adyVer + 1` al miembro `puntos` del jugador en
               posición `i` de arreglo `jugs`.
         1. Si `adHor` es igual a `0` y `adVer` es igual a `0:
            1. Incremento `1` al miembro `puntos` del jugador en posición `i`
               de arreglo `jugs`.
         1. Incremento a posición `azCol - 1`  de arreglo `basu` la cantidad
            `j` (un azulejo ha sido ubicado en `pared`) de azulejos.
         1. Asigno `0` a la `cantidad` de azulejos del patrón `j`.
         1. Asigno `VACIO` a `colorAzulejo` de azulejos del patrón `j`.
         1. Si retorno de `sumarFila()` para fila `j` de pared de jugador en
            posición `i` es igual a `15`:
            1. Asigno verdadero a `finPartida`.
   1. Declaro `pena` y asigno retorno de `calcularPenalizaciones()`.
   1. Si `pena` es menor a miembro `puntos` de jugador en posición `i` de
      arreglo `jugs`:
      1. Resto a `puntos` el valor de `pena`.
   1. De lo contrario (más penalizaciones que puntos):
      1. Asigno `0` a `puntos`.
1. Invoco `imprimirResultados()` para visualizar jugadores ordenados por
   puntaje descendente.
1. Retorno valor de `finPartida`.

### Función: `imprimirResultados`

Muestra en pantalla lista con nombre de jugadores y respectivo puntaje; orden
descendente sobre puntaje.

#### Entradas

- `jugs`: arreglo constante de tipo `Jugador` de tamaño `CANT_JUGS`.

#### Salidas

Ninguna (impresión en pantalla).

#### Pseudo-código

1. Declaro arreglo `aux` de enteros positivos de tamaño `CANT_JUGS`.
1. Para `i` desde `0`; hasta `i` menor a `CANT_JUGS`, incremento `i` en `1`:
   1. Asigno `i` a la posición `i` de arreglo `aux`.
1. Imprimo encabezado de tabla.
1. Para `i` desde `0`; hasta `i` menor a `CANT_JUGS - 1`, incremento `i` en
   `1`:
   1. Declaro entero positivo `iMax` y asigno valor de posición `aux[i]`.
   1. Para `j` desde `i`; hasta `j` menor a `CANT_JUGS`, incremento `j` en `1`:
      1. Si miembro `puntos` de jugador en posición `iMax` de arreglo `jugs`es
         menor a miembro `puntos` de jugador en posición `aux[j]` de arreglo
         `jugs`:
         1. Asigno el valor de  posición `aux[j]` a `iMax`.
   1. Si `iMax` es diferente a `aux[i]` (se encontró un nuevo mayor):
      1. Declaro `iSwap` y asigno valor de `aux[i]`.
      1. Asigno `aux[iMax]` a `aux[i]`
      1. Asigno `iSwap` a `aux[iMax]`
   1. Imprimo: "`i + 1`. `jugs[i].nombre` `jugs[i].puntos`
   1. Imprimo cambio de línea.

### Función: `calcularPuntajeFinal`

Agrega puntos si pared de jugador:

- 2 puntos por fila completa.
- 7 puntos por columna completa.
- 10 puntos por color completo.

#### Entradas

- `jugs`: arreglo de tipo `Jugador` de tamaño `CANT_JUGS`.

#### Salidas

Ninguna (modificación sobre valores por referencia).

#### Pseudo-código

1. Para `k` desde `0`; hasta `k` menor a `CANT_JUGS`; incremento `k` en `1`:
   1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en `1`:
      1. Declaro booleano `filLlena` y asigno verdadero.
      1. Declaro booleano `colLlena` y asigno verdadero.
      1. Declaro booleano `colComp` y asigno verdadero.
      1. Para `j` desde `0`; hasta `j` menor a `5`; incremento `j` en `1`:
         1. Si elemento en posición `[i][j]` de matriz `pared` de jugador en
            posición `k` de arreglo `jugs` es igual a `VACIO`:
            1. Asigno falso a `filLlena`.
         1. Si elemento en posición `[j][i]` de matriz `pared` de jugador en
            posición `k` de arreglo `jugs` es igual a `VACIO`:
            1. Asigno falso a `colLlena`.
            1. Si  elemento en posición `[j][(i + j) % 5] es igual a `VACIO`:
               1. Asigno falso a `colComp`.
      1. Si `filLlena` es verdadero:
         1. Incremento `2` a miembro `puntos` de jugador en posición `k` de
            arreglo `jugs.
      1. Si `colLlena` es verdadero:
         1. Incremento `7` a miembro `puntos` de jugador en posición `k` de
            arreglo `jugs.
      1. Si `colComp` es verdadero:
         1. Incremento `10` a miembro `puntos` de jugador en posición `k` de
            arreglo `jugs.

### Función estática: `escogerPatron`

Imprime en pantalla los patrones vacíos sin azulejo de color coincidente en
fila de pared y obtiene de usuario el patrón escogido. Si no se encuentra
patrón valido, retorna `-2`.

#### Entradas

- `azCol`: enumeración constate de tipo `Color`.
- `patrPtr`: estructura constante de tipo `Patrones`.
- `pared`: matriz constante de tamaño `5` por `5` con posiciones ocupadas.

#### Salida

Número de línea de patrón escogida por usuario, -2 si no se encuentra patrón
válido.

#### Pseudo-código

1. Declaro arreglo de enteros positivos `patrVal` para almacenar posición de
   patrones válidos e inicializo con `false`.
1. Imprimo: "Buscando patrones válidos..."
1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en 1:
   1. Si elemento `i` de arreglo/miembro `colorAzulejo` de estuctura `patrPtr`
      es igual a `VACIO` y retorno de `buscarColor()` para color `azCol` en
      fila `i` de matriz `pared` es igual a `-1`:
      1. Asigno `true` a elemento `i` de arreglo `patrVal`.
      1. Imprimo: "`i + 1`. ".
   1. De lo contrario (fila sin patrón válido).
      1. Imprimo: "X. ".
   1. Invoco `impFilaPatron()` para fila `i`, color en posición `i` de
      `colorAzulejo` y cantidad en posición `i`.
   1. Imprimo tabulación.
   1. Invoco `impFilaPared()` para arreglo de la fila `i` de `pared`.
   1. Imprimo cambio de línea.
1. Si retorno de `sumarFila()` para arreglo `patrVal` es igual a `0`:
   1. Imprimo: "No se encontró patrón válido para jugada..."
   1. Retorno `-2`.
1. Declaro entero para posición de línea `lin`.
1. Hacer:
   1. Imprimo: "Escoge número de patrón válido (entre 1 y 5): ".
   1. Obtengo de usuario entero positivo y asigno a `lin`.
   1. Mientras `lin` sea menor a `1` o mayor a `5` o elemento `lin - 1` de
      arreglo `patrVal` sea igual a `0`.
1. Decremento `lin` en `1` (coincidencia posición de patrón).
1. Retorno `lin`.

### Función estática: `impFilaPatron`

Imprime en pantalla un fila de patrones. Agrega espacios a la izquierda de la
línea antes de imprimir azulejos o casilla vacía.

#### Entradas

- `fil`: entero positivo para indicar número de línea (entre `0` y `4`).
- `azCol`: enumeración de tipo `Color`.
- `azCan`: entero positivo con cantidad de azulejos.

#### Salida

Ninguna (impresión en pantalla).

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `4 - fil`; incremento `i` en `1`:
   1. Imprimo un espacio.
1. Para `i` desde `0`; hasta `i` menor a `fil + 1 - azCan`; incremento `i` en
   `1`:
   1. Invoco `imprimirAzulejo()` para `azCol * (-1)`.
1. Para `i` desde `0`; hasta `i` menor a `azCan`; incremento `i` en `1`:
   1. Invoco `imprimirAzulejo()` para `azCol`.

### Función estática: `impFilaPared`

Imprime en pantalla un fila de pared.

#### Entradas

- `fil`: entero positivo para indicar número de línea (entre `0` y `4`).
- `parFil`: arreglo constante de tamaño `5` indicando casillas con azulejo
  usando enumeración de tipo `Color`.

#### Salida

Ninguna (impresión en pantalla).

#### Pseudo-código

1. Para `j` desde `0`; hasta `j` menor a `5`; incremento `j` en `1`:
   1. Si elemento en columna `j` de `parFil` es igual a `VACIO`:
      1. Invoco `imprimirAzulejo()` para `MOSAICO[fil][j] * (-1)`.
   1. De lo contrario (casilla con azulejo):
      1. Invoco `imprimirAzulejo()` para `MOSAICO[fil][j]`.

### Función estática: `buscarColor`

Busca sobre arreglo de tamaño `5` la posición de un `Color`. Si no encuentra
color, retorna `-1`.

#### Entradas

- `arr`: arreglo de tamaño `5` con enumeraciones de tipo `Color`.
- `azCol`: enumeración de tipo `Color`.

#### Salida

Posición del color buscado, si no se encuentra retorna `-1`.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en `1`:
   1. Si elemento `i` de arreglo `arr` es igual a color `azCol`:
      1. Retorno `i`.
1. Retorno `-1`.

### Función estática: `contarAdyacentes`

Cuenta azulejos adyacentes en una dirección dada una posición inicial.

#### Entradas

- `pared`: matriz constante de tamaño `5` por `5` con enumeraciones de tipo
  `Color`.
- `fil`: índice (entero positivo) de fila de posición inicial de azulejo.
- `col`: índice (entero positivo) de columna de posición inicial de azulejo.
- `movX`: movimiento (entero) en eje horizontal.
- `movY`: movimiento (entero) en eje vertical.

#### Salida

Cantidad de azulejos adyacentes en dirección dada.

#### Pseudo-código

1. Declaro entero positivo `cont` y asigno `0`.
1. Mientras `fil + movY` esté entre `0` y `4` (inclusivo) y `col + movX` esté
   entre `0` y `4` y elemento en posición `[fil + movY][col + movX]` es
   diferente de `VACIO`:
   1. Incremento `cont` en `1`.
   1. Incremento `fil` con valor de `movY`.
   1. Incremento `col` con valor de `movX`.
1. Retorno `cont`.

### Función estática: `calcularPenalizaciones`

Retorna cantidad de puntos negativos de acuerdo a cantidad de azulejos en piso
y asigna cero a piso (referencia).

#### Entradas

- `pisoPtr`: contador de penalizaciones (entre `0` y `7`) (referencia).

#### Salida

Entero positivo con cantidad de puntos negativos.

#### Pseudo-código

1. Declaro entero positivo `pena`.
1. Si `pisoPtr` es igual a `1` o a `2`:
   1. Asigno `pisoPtr` a `pena`.
1. De lo contrario, si `pisoPtr` es igual a `3`:
   1. Asigno `4` a `pena`.
1. De lo contrario, si `pisoPtr` es igual a `4`:
   1. Asigno `6` a `pena`.
1. De lo contrario, si `pisoPtr` es igual a `5`:
   1. Asigno `8` a `pena`.
1. De lo contrario, si `pisoPtr` es igual a `6`:
   1. Asigno `11` a `pena`.
1. De lo contrario, si `pisoPtr` es igual a `7`:
   1. Asigno `14` a `pena`.
1. De lo contrario (no hay penalizaciones):
   1. Asigno `0` a `pena`.
1. Asigno 0 a `pisoPtr`.
1. Retorno `pena`.

## Acciones sobre proveedor: `proveedor.c`

### Función: `iniciarPartida`

Asigna `20` azulejos por color en fila `0` (bolsa) de miembro `azulejos` de la
partida, todos los otros elementos de matriz se asignan a `0`. Se asigna
verdadero a marcador de turno.

#### Entradas

- `part`: estructura de tipo `Proveedor` con distribución central de azulejos
  (referencia).

#### Salida

- Ninguna.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `(FIL_AZU) * 5`; incremento `i`
   en `1`:
   1. Si `i / 5` es diferente de cero:
      1. Asigno `0` a elemento en fila `i / 5` y columna `i % 5` de miembro
         `azulejos`.
   1. De lo contrario (fila `0`, bolsa):
      1. Asigno `20` a elemento en fila `i / 5` y columna `i % 5` de miembro
         `azulejos`.
1. Asigno `true` a miembro `marcadorTurno`.

### Función: `surtirMostradores`

Asigna `4` azulejos aleatorios en cada mostrador y activa marcador de turno en
centro de mesa.

#### Entradas

- `part`: estructura de tipo `Proveedor` con distribución central de azulejos
  (referencia).

#### Salida

- Ninguna.

#### Pseudo-código

1. Declaro entero positivo `cantBolsa` y asigno retorno de `sumarFila()` para
   fila `0`.
1. Si `cantBolsa` es menor a `20`:
   1. Invoco `volcarFila()` para incrementar azulejos disponibles en bolsa
      volcando azulejos en basura.
   1. Asigno a `cantBolsa` de retorno de `sumarFila()` para fila `0`.
   1. Si `cantBolsa` es menor a `20`:
      1. Imprimo: "Advertencia: no hay azulejos suficientes en bolsa para una
         nueva ronda".
      1. Imprimo: "Surtiendo mostradores incompletos...".
1. Para `i` desde `3`; hasta `i` menor a `FIL_AZU`; incremento `i` en `1`:
   1. Para `j` desde `0`; hasta `j` menor a `4` o `cantBolsa` mayor a `0`;
      incremento `j` en `1`:
      1. Declaro entero positivo `jCo`.
      1. Hacer:
         1. Asigno valor aleatorio entre `0` y `4` (inclusivo) a `jCo`.
         1. Mientras valor de elemento en fila cero (bolsa) y columna `jCo` es
            igual a `0`.
      1. Incremento en `1` el elemento en la fila `i` y columna `jCo` de
         `part.azulejos`.
      1. Decremento en `1` el elemento en la fila `0` (bolsa) y columna `jCo`
         de `part.azulejos`.
      1. Decremento en `1` `cantBolsa`
1. Asignar `true` a miembro `marcadorTurno` de estructura `part`.

### Función: `proveedorVacio`

Retorna verdadero si no hay azulejos en centro de mesa ni mostradores, de lo
contrario falso.

#### Entradas

- `azs`: matriz constante con distribución central de azulejos.

#### Salida

Verdadero si centro de mesa y mostradores están vacíos, de lo contrario falso.

#### Pseudo-código

1. Para `i` desde `2`; hasta `i` menor a `FIL_AZU`; incremento `i` en `1`:
   1. Si `sumarFila()` para fila `i` de matriz `azs` es mayor a `0`:
      1. Retorno falso.
1. Retorno verdadero.

### Función: `escogerAzulejos`

Lanza subrutinas para seleccionar fuente (centro de mesa o algún mostrador) y
color de azulejo(s). Si la fuente es centro de mesa y se encuentra marcador de
primer turno retorna verdadero, de lo contrario falso.

#### Entradas

- `partPtr`: estructura de tipo `Proveedor` con distribución central de
  azulejos (referencia).
- `azCol`: variable de tipo enumeración `Color`, representa color de azulejo(s)
  a seleccionar (referencia).
- `azCan`: entero positivo con cantidad de azulejos a seleccionar (referencia).

#### Salida

Verdadero si se escoge centro de mesa por primera vez en la ronda, de lo
contrario falso.

#### Pseudo-código

1. Declaro booleano `marcIni` y asigno falso.
1. Declaro entero positivo `selFuente` y asigno retorno de `escogerFuente()`.
1. Si fuente de azulejos `selFuente` es el centro de mesa (igual a `2`) y el
   centro de mesa contiene marcador de turno (miembro `marcadorTurno` es
   verdadero):
   1. Asigno verdadero a `marcIni`.
   1. Asigno falso a miembro `marcadorTurno` de estructura `partPtr`.
1. Asigno retorno de `escogerColor()` a `azCol` para fuente seleccionada
   `selFuente`.
1. Asigno a `azCan` el valor del elemento en fila `selFuente` y columna
   `*azColPtr - 1` de miembro `azulejos` en estructura `partPtr`.
1. Asigno `0` al elemento en fila `selFuente` y columna `*azColPtr - 1` de
   miembro `azulejos` en estructura `partPtr`.
1. Si `selFuente` es un mostrador (entre `3` y `FIL_AZU`):
   1. Invoco `volcarFila()` con origen fila `selFuente` y destino fila `2`
      (centro de mesa) de miembro `azulejos` en estructura `partPtr`.
1. Retorno `marcIni`.


### Función estática: `volcarFila`

Suma elemento a elemento valores entre dos arreglos (filas) de tamaño `5`.
Asigna resultados a arreglo de destino y asigna ceros a arreglo de origen.

#### Entradas

- `orig`: arreglo con números enteros positivos de tamaño `5`.
- `dest`: arreglo con números enteros positivos de tamaño `5`.

#### Salida

- Ninguna.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en `1`:
   1. Sumo a elemento en posición `i` de arreglo `dest` el elemento en
      posición `i` de arreglo `orig`.
   1. Asigno `0` a elemento en posición `i` de arreglo `orig`.

### Función estática: `escogerFuente`

Pregunta a usuario por fuente de azulejos (centro de mesa o algún mostrador)
hasta obtener una opción válida (entre `1` y `CANT_MOST + 1`) que no esté
vacía.

#### Entradas

- `part`: apuntador a estructura constante de tipo `Proveedor` con distribución
  central de azulejos .

#### Salida

Número de fila que representa fuente de azulejos en miembro `azulejos` de
estructura de tipo `Proveedor`.

#### Pseudo-código

1. Declaro entero positivo `fuente`.
1. Hacer:
   1. Invoco `imprimirProveedor()`.
   1. Pregunto a usuario por selección de fuente de azulejos (uno de los
      mostradores entre `1` y `CANT_MOST` o `CANT_MOST + 1` centro de mesa).
   1. Obtengo de usuario selección de fuente de azulejos y asigno a `fuente`.
   1. Mientras `fuenteValida()` para `fuente` (fila de matriz miembro
      `azulejos`) retorne falso.
1. Retorno `fuente`.

### Función estática: `imprimirProveedor`

Imprime en pantalla azulejos contenidos en mostradores y centro de mesa.
Además muestra si marcador de turno se encuentra en centro de mesa.

#### Entradas

- `partPtr`: apuntador a estructura constante de tipo `Proveedor` con
  distribución central de azulejos.

#### Salida

Ninguna.

#### Pseudo-código

1. Para `i` igual a `3`; hasta `i` menor a `FIL_AZU`; incremento `i` en `1`:
   1. Imprimo: "`i - 2`. Mostrador `i - 2`: ".
   1. Invoco `imprimirFuente()` para fila `i` de miembro `azulejos`.
   1. Imprimo cambio de línea.
1. Imprimo: "`FIL_AZU - 2`. Centro de mesa:"
1. Invoco `imprimirFuente()` para fila `2` de miembro `azulejos`.
1. Imprimo cambio de línea.
1. Imprimo: "   Marcador:"
1. Si miembro `marcadorTurno` es verdadero:
   1. Imprimo: "Sí"
1. De lo contrario:
   1. Imprimo: "No"
1. Imprimo cambio de línea.

### Función estática: `imprimirFuente`

Imprime en pantalla azulejos contenidos en centro de mesa o un mostrador.

#### Entradas

- `fila`: arreglo de tamaño `5` con cantidad de azulejos por posición/color.

#### Salida

Ninguna.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en `1`:
   1. Declaro entero positivo `azCan` y asigno valor de elemento en posición
      `i` de arreglo `fila`.
   1. Mientras `azCan` es mayor a `0`:
      1. Invoco `imprimirAzulejo()` para color `i + 1` (coincidencia con
         enumeración de tipo `Color`).
      1. Decremento `azCan` en `1`.

### Función estática: `fuenteValida`

Verifica si fuente es válida (entre `1` y `CANT_MOST + 1`), convierte la fuente
en número de fila y consulta si la fuente no está vacía. De lo contrario
retorna falso.

#### Entradas

- `azs`: estructura constante de tipo `Proveedor` con distribución central de
  azulejos.
- `fuPtr`: fuente a evaluar, si es válida se asigna número de fila
  (referencia).

#### Salida

Verdero si fuente de azulejos es válida, de lo contrario falso.

#### Pseudo-código

1. Si `*fuPtr` es menor a `1` o mayor a `CANT_MOST + 1`:
   1. Imprimir: "Fuente no válida, por favor ingrese un valor entre 1 y
      `CANT_MOST + 1`".
   1. Retorna falso.
1. Si `*fuPtr` es igual a `CANT_MOST + 1`:
   1. Asigno número de fila `2` a `*fuPtr`.
1. De lo contrario (no es centro de mesa):
   1. Sumo `2` a `*fuPtr` para convertir fuente en número de fila.
1. Si retorno de `sumarFila()` para fila `*fuPtr` de matriz `azs` es menor a
   `1`:
   1. Imprimir: "Fuente vacía, por favor escoja otra fuente".
   1. Retorna falso.
1. Retorna verdadero.

### Función estática: `escogerColor`

Retorna color escogido por usuario disponible en fuente.

#### Entradas

- `azs`: matriz constante de números enteros positivos con distribución central
  de azulejos .
- `fuente`: entero positivo que representa  número de fila que contiene fuente
  de azulejos en matriz `azs`.

#### Salida

Enumerpación de tipo `Color` representa color (comlumna + `1`) de azulejo(s)
escogido.

#### Pseudo-código

1. Declaro carácter `colChar`.
1. Declaro `azCol` de tipo `Color` para almacenar el color de azulejo.
1. Hacer:
   1. Invoco `imprimirFuente()` para fila `fuente` en matriz `azs`.
   1. Imprimo cambio de línea.
   1. Si `fuente` es diferente de `2`:
      1. Imprimo: "Escoge un color de mostrador `fuente - 2`:".
   1. De lo contrario (centro de mesa):
      1. Imprimo: "Escoge un color de centro de mesa:".
   1. Obtengo de usuario selección de color de azulejo(s) y asigno a `colChar`.
   1. Mientras `colorValido()` para `colChar` retorne falso, con `azCol` como
      referencia y `fuente` seleccionada.

### Función estática: `colorValido`

Verifica si carácter ingresado por usuario representa un color disponible en
fuente.

#### Entradas

- `colChar`: carácter ingresado por usuario para selección de color.
- `fila`: arreglo constante de enteros positivos de tamaño `5`  que contiene
  cantidad de azulejos por color de la fuente seleccionada.
- `azColPtr`: puntero a enumeración de tipo `COLOR` que representa número de
  columna (mas `1`) que representa color de azulejos en el miembro `azulejos`
  de la estructura `part` (referencia).

#### Salida

Verdadero si carácter ingresado por usuario representa un color disponible en
fuente, de lo contrario retorna falso.

#### Pseudo-código

1. Si `colChar` es igual a `A` o `a`:
   1. Asigno `AZUL` a `azColPtr`.
1. De lo contrario, si `colChar` es igual a `R` o `r`:
   1. Asigno `ROJO` a `azColPtr`.
1. De lo contrario, si `colChar` es igual a `M` o `m`:
   1. Asigno `MORADO` a `azColPtr`.
1. De lo contrario, si `colChar` es igual a `N` o `n`:
   1. Asigno `NEGRO` a `azColPtr`.
1. De lo contrario, si `colChar` es igual a `B` o `b`:
   1. Asigno `BLANCO` a `azColPtr`.
1. De lo contrario (carácter inválido):
   1. Imprimir: "La entrada `colChoar` no es válida.".
   1. Imprimir: "Por favor intenta nuevamente.".
   1. Retorno falso.
1. Si elemento en la posición `*azColPtr - 1` del arreglo `fila` es menor a `1`:
   1. Imprimir: "No se encuentran azulejos del color seleccionado.".
   1. Imprimir: "Por favor intenta nuevamente.".
   1. Retorno falso.
1. Retorno verdadero.

## Acciones comunes: `utils.c`

### Función: `sumarFila`

Retorna la suma entre elementos de un arreglo (fila) de tamaño `5`.

#### Entradas

- `arr`: arreglo constante de  números enteros.

#### Salida

- Entero positivo que representa cantidad de azulejos contenido en bolsa.

#### Pseudo-código

1. Declaro entero positivo `sum` y asigno `0`.
1. Para `i` desde `0`; hasta `i` menor a `5`; incremento `i` en `1`:
   1. Sumo elemento en posición `i` de arreglo `arr` a `sum`.
1. Retorno valor de `sum`.

### Función: `imprimirAzulejo`

Imprime en pantalla carácter que representa azulejo usando la siguiente
correlación:

| Entrada | Representa   | Salida    |
| ---:    | :---         | :---:     |
| `0`     | Vacío        | `\|   \|` |
| `1`     | Azul lleno   | `\| A \|` |
| `-1`    | Azul vacío   | `\| a \|` |
| `2`     | Rojo lleno   | `\| R \|` |
| `-2`    | Rojo vacío   | `\| r \|` |
| `3`     | Morado lleno | `\| M \|` |
| `-3`    | Morado vacío | `\| m \|` |
| `4`     | Negro lleno  | `\| N \|` |
| `-4`    | Negro vacío  | `\| n \|` |
| `5`     | Blanco lleno | `\| B \|` |
| `-5`    | Blanco vacío | `\| b \|` |

#### Entradas

- `azCol`: número entero que representa color o ausencia de azulejo (entre
  `-5` y `5`).

#### Salida

Ninguna (impresión en pantalla).

#### Pseudo-código

1. Si `azCol` es igual `0`:
   1. Imprimo: "|   |"
1. Si `azCol` es igual `1`:
   1. Imprimo: "| A |"
1. Si `azCol` es igual `-1`:
   1. Imprimo: "| a |"
1. Si `azCol` es igual `2`:
   1. Imprimo: "| R |"
1. Si `azCol` es igual `-2`:
   1. Imprimo: "| r |"
1. Si `azCol` es igual `3`:
   1. Imprimo: "| M |"
1. Si `azCol` es igual `-3`:
   1. Imprimo: "| m |"
1. Si `azCol` es igual `4`:
   1. Imprimo: "| N |"
1. Si `azCol` es igual `-4`:
   1. Imprimo: "| n |"
1. Si `azCol` es igual `5`:
   1. Imprimo: "| B |"
1. Si `azCol` es igual `-5`:
   1. Imprimo: "| b |"

## Referencias

[^1]: https://planbgames.com/games/azul-next-move-games-michael-kiesling-strategy-abstract-board-game-winner-spiel-des-jahres-game-of-the-year-cannes-portuguese-tiles-royal-palace-of-evora-1
