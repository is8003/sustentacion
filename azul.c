#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utils.h"
#include "proveedor.h"
#include "jugador.h"

static void jugarRonda(Jugador jugs[], Proveedor * partPtr, unsigned iJug);
static void cargarParedes(Jugador jugs[]);

const Color MOSAICO[5][5] = {
	{ AZUL, ROJO, MORADO, NEGRO, BLANCO },
	{ BLANCO, AZUL, ROJO, MORADO, NEGRO },
	{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
	{ MORADO, NEGRO, BLANCO, AZUL, ROJO },
	{ ROJO, MORADO, NEGRO, BLANCO, AZUL }
};

int main(void){
	Proveedor partida;
	Jugador jugadores[CANT_JUGS];

	// Sustentación
	cargarParedes(jugadores);
	calcularPuntajeFinal(jugadores);
	imprimirResultados(jugadores);

	srand(time(NULL));

	iniciarPartida(&partida);
	iniciarJugadores(jugadores);

	unsigned jugadorInicial;

	do {
		surtirMostradores(&partida);
		partida.marcadorTurno = true;
		jugadorInicial = escogerIniciadorRonda(jugadores);
		jugarRonda(jugadores, &partida, jugadorInicial);
	} while (!calcularPuntaje(jugadores,partida.azulejos[1]));

	calcularPuntajeFinal(jugadores);
	imprimirResultados(jugadores);

	exit(EXIT_SUCCESS);
}

static void jugarRonda(Jugador jugs[], Proveedor * partPtr, unsigned iJug){
	puts("\nInicio de ronda...");

	while (!proveedorVacio((const unsigned (*)[])partPtr->azulejos)){
		Color azCol = VACIO;
		unsigned azCan = 0, azDes = 0;

		printf("\n\nTurno de jugador %u - %s\n", iJug + 1,
				jugs[iJug].nombre);

		printf("\nImprimiendo tablero de jugador %u - %s", iJug + 1,
				jugs[iJug].nombre);
		imprimirTablero(&jugs[iJug]);

		puts("\nImprimiendo proveedor...");
		if (escogerAzulejos(partPtr, &azCol, &azCan)){
			jugs[iJug].primerTurno = true;
			agregarPenalizacion(&jugs[iJug].piso, 1);
		}

		printf("\nImprimiendo tablero de jugador %u - %s", iJug + 1,
				jugs[iJug].nombre);
		imprimirTablero(&jugs[iJug]);

		azDes = ubicarAzulejos(azCol, azCan, &jugs[iJug].lineas,
				jugs[iJug].pared, &jugs[iJug].piso);

		if (azDes > 0){
			partPtr->azulejos[1][azCol - 1] += azDes;
		}

		printf("\nImprimiendo tablero de jugador %u - %s", iJug + 1,
				jugs[iJug].nombre);
		imprimirTablero(&jugs[iJug]);

		iJug =  (iJug + 1) % CANT_JUGS;
	}

	puts("\nFin de ronda...");
}

static void cargarParedes(Jugador jugs[]){
	FILE * fp = fopen("paredes.txt", "r");

	if (fp == NULL){
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	puts("Leyendo contenidos de archivo de texto...\n");

	for (size_t k = 0; k < CANT_JUGS; k++){
		if (fscanf(fp, "%19s", jugs[k].nombre) != 1){
			fprintf(stderr, "fscanf: error al leer el "\
					"nombre de jugador\n");

			fclose(fp);
			exit(EXIT_FAILURE);
		}

		jugs[k].puntos = 0;

		for (size_t i = 0; i < 25; i++){
			unsigned e;

			if (fscanf(fp, "%u", &e) != 1){
				fprintf(stderr, "fscanf: error al leer el dato "\
						"[%zd][%zd]\n", i / 5, i % 5);

				fclose(fp);
				exit(EXIT_FAILURE);
			}

			jugs[k].pared[i / 5][i % 5] = e;
		}
	}

	fclose(fp);
}
