#include <stdio.h>
#include <stdlib.h>
#include "jugador.h"

static unsigned calcularPenalizaciones(unsigned * pisoPtr);
static unsigned contarAdyacentes(const Color pared[][5], unsigned fil,
		unsigned col, const int movX, const int movY);
static int buscarColor(const Color arr[], const Color azCol);
static void impFilaPared(const Color parFil[], const unsigned fil);
static void impFilaPatron(const unsigned fil, const Color azCol,
		const unsigned azCan);
static int escogerPatron(const Color azCol, const Patrones * patrPtr,
		const Color pared[][5]);

static unsigned calcularPenalizaciones(unsigned * pisoPtr){
	unsigned pena;

	switch (*pisoPtr){
		case 1:
		case 2:
			pena = *pisoPtr;
			break;
		case 3:
			pena = 4;
			break;
		case 4:
			pena = 6;
			break;
		case 5:
			pena = 8;
			break;
		case 6:
			pena = 11;
			break;
		case 7:
			pena = 14;
			break;
		default:
			pena = 0;
	}

	*pisoPtr = 0;

	return pena;
}

static unsigned contarAdyacentes(const Color pared[][5], unsigned fil,
		unsigned col, const int movX, const int movY){
	unsigned cont = 0;

	while ((int)fil + movY >= 0 && (int)fil + movY < 5 &&
			(int)col + movX >= 0 && (int)col + movX < 5 &&
			pared[fil + movY][col + movX] != VACIO){
		cont++;
		fil += movY;
		col += movX;
	}

	return cont;
}

static int buscarColor(const Color arr[], const Color azCol){
	for (size_t i = 0; i < 5; i++){
		if ( azCol == arr[i]){
			return (int)i;
		}
	}

	return -1;
}

static void impFilaPared(const Color parFil[], const unsigned fil){
	for (size_t j = 0; j < 5; j++){
		if (parFil[j] == VACIO){
			imprimirAzulejo(MOSAICO[fil][j] * (-1));
		}
		else {
			imprimirAzulejo(MOSAICO[fil][j]);
		}
	}
}

static void impFilaPatron(const unsigned fil, const Color azCol,
		const unsigned azCan){
	for (size_t i = 0; i < 4 - fil; i++){
		printf("   ");
	}

	for (size_t i = 0; i < fil + 1 - azCan; i++){
		imprimirAzulejo(azCol * (-1));
	}

	for (size_t i = 0; i < azCan; i++){
		imprimirAzulejo(azCol);
	}
}

static int escogerPatron(const Color azCol, const Patrones * patrPtr,
		const Color pared[][5]){
	unsigned patrVal[5] = {0};

	puts("Buscando patrones válidos...");
	for (size_t i = 0; i < 5; i++){
		if (patrPtr->colorAzulejo[i] == VACIO &&
				buscarColor(pared[i], azCol) == -1){
			patrVal[i] = 1;
			printf("%zu. ", i + 1);
		}
		else {
			printf("%s", "X. ");
		}

			impFilaPatron(i, patrPtr->colorAzulejo[i],
					patrPtr->cantidad[i]);
			printf("%s", "\t");
			impFilaPared(pared[i],i);
			puts("");
	}

	if (sumarFila(patrVal) == 0){
		puts("\nNo se encontró patrón válido para jugada...");
		return -2;
	}

	unsigned lin;

	do{
		printf("Escoge número de patrón válido: ");
		scanf("%u", &lin);
	}while (lin < 1 || lin > 5 || patrVal[lin - 1] == 0);

	return --lin;
}

void calcularPuntajeFinal(Jugador jugs[]){
	for (size_t k = 0; k < CANT_JUGS; k++){
		for (size_t i = 0; i < 5; i++){
			bool filLlena = true;
			bool colLlena = true;
			bool colComp = true;

			for (size_t j = 0; j < 5; j++){
				if (filLlena && jugs[k].pared[i][j] == VACIO){
					filLlena = false;
				}

				if (colLlena && jugs[k].pared[j][i] == VACIO){
					colLlena = false;
				}


				if (colComp && jugs[k].pared[j][(i + j) % 5]
						== VACIO){
					colComp = false;
				}
			}

			if (filLlena){
				jugs[k].puntos += 2;
			}

			if (colLlena){
				jugs[k].puntos += 7;
			}

			if (colComp){
				jugs[k].puntos += 10;
			}
		}
	}
}

bool calcularPuntaje(Jugador jugs[], Color basu[]){
	bool finPartida = false;

	for (size_t i = 0; i < CANT_JUGS; i++){
		for (size_t j = 0; j < 5; j++){
			if ( j + 1 == jugs[i].lineas.cantidad[j]){
				Color azCol = jugs[i].lineas.colorAzulejo[j];
				unsigned col = buscarColor(MOSAICO[j], azCol);

				jugs[i].pared[j][col] = azCol;

				unsigned adyHor = 0, adyVer = 0;

				adyHor += contarAdyacentes(
						(const Color (*)[])jugs[i].pared,
						j, col, 1, 0);
				adyHor += contarAdyacentes(
						(const Color (*)[])jugs[i].pared,
						j, col, -1, 0);

				adyVer += contarAdyacentes(
						(const Color (*)[])jugs[i].pared,
						j, col, 0, 1);
				adyVer += contarAdyacentes(
						(const Color (*)[])jugs[i].pared,
						j, col, 0, -1);

				if (adyHor > 0){
					jugs[i].puntos += adyHor + 1;
				}

				if (adyVer > 0){
					jugs[i].puntos += adyVer + 1;
				}

				if (adyHor == 0 && adyVer == 0){
					jugs[i].puntos++;
				}

				basu[azCol - 1] += j;
				jugs[i].lineas.cantidad[j] = 0;
				jugs[i].lineas.colorAzulejo[j] = VACIO;

				if (sumarFila(jugs[i].pared[j]) == 15){
					finPartida = true;
				}
			}
		}

		unsigned pena = calcularPenalizaciones(&jugs[i].piso);

		if (pena < jugs[i].puntos){
			jugs[i].puntos -= pena;
		}
		else {

			jugs[i].puntos = 0;
		}
	}

	imprimirResultados(jugs);

	return finPartida;
}

void imprimirResultados(const Jugador jugs[]){
	unsigned aux[CANT_JUGS];

	for (size_t i = 0; i < CANT_JUGS; i++){
		aux[i] = i;
	}

	puts("\nPos. | Nombre              | Puntos |");

	for (size_t i = 0; i < CANT_JUGS - 1; i++){
		unsigned iMax = aux[i];
		for (size_t j = i; j < CANT_JUGS; j++){
			if (jugs[iMax].puntos < jugs[aux[j]].puntos){
				iMax = aux[j];
			}
		}

		if (iMax != aux[i]){
			unsigned iSwap = aux[i];
			aux[i] = aux[iMax];
			aux[iMax] = iSwap;
		}

		printf("%3zu. | %19s | %6u |\n", i + 1, jugs[aux[i]].nombre,
				jugs[aux[i]].puntos);
	}

	printf("%3u. | %19s | %6u |\n", CANT_JUGS,
			jugs[aux[CANT_JUGS - 1]].nombre,
			jugs[aux[CANT_JUGS - 1]].puntos);
}

void agregarPenalizacion(unsigned * pisoPtr, const unsigned pena){
	if (pena + *pisoPtr > 7){
		*pisoPtr = 7;
	}
	else {
		*pisoPtr += pena;
	}
}

unsigned ubicarAzulejos(const Color azCol, unsigned azCan,
		Patrones * patrPtr, Color pared[][5], unsigned * pisoPtr){
	int lin = buscarColor(patrPtr->colorAzulejo, azCol);

	if (lin == -1){
		lin = escogerPatron(azCol,patrPtr,(const Color (*)[])pared);
	}
	else {
		printf("Color disponible en línea de patrón No. %d...\n",
				lin + 1);
	}

	if (lin != -2){
		printf("Ubicando azulejos en línea de patrón No. %d...\n",
				lin + 1);
                patrPtr->colorAzulejo[lin] = azCol;
		if (patrPtr->cantidad[lin] + azCan <= (unsigned)lin + 1){
			patrPtr->cantidad[lin] += azCan;
			azCan = 0;
		}
		else {
			azCan -= lin + 1 - patrPtr->cantidad[lin];
			patrPtr->cantidad[lin] = lin + 1;
			printf("Sobra %u azulejos...\n", azCan);
			agregarPenalizacion(pisoPtr, azCan);
		}
	}
	else {

			printf("Sobra %u azulejos...\n", azCan);
			agregarPenalizacion(pisoPtr, azCan);
	}

	return azCan;
}

void imprimirTablero(const Jugador * jug){
	printf("\nPuntaje: %u\n", jug->puntos);

	for (size_t i = 0; i < 5; i++){
		impFilaPatron(i, jug->lineas.colorAzulejo[i],
				jug->lineas.cantidad[i]);
		printf("\t");
		impFilaPared(jug->pared[i], i);
		puts("");
	}

	printf("Penalizaciones: %u\n", jug->piso);
	printf("Primer turno: %s\n", (jug->primerTurno) ? "Sí" : "No");
}

unsigned escogerIniciadorRonda(Jugador jugs[]){
	for (size_t i = 0; i < CANT_JUGS; i++){
		if (jugs[i].primerTurno){
			jugs[i].primerTurno = false;
			return i;
		}
	}

	return rand() % CANT_JUGS;
}

void iniciarJugadores(Jugador jugs[]){
	for (size_t i = 0; i < CANT_JUGS; i++){
		printf("Jugador %zu, ingrese su nombre: ", i + 1);
		scanf("%19s", jugs[i].nombre);

		jugs[i].puntos = 0;

		for (size_t j = 0; j < 5; j++){
			jugs[i].lineas.colorAzulejo[j] = VACIO;
			jugs[i].lineas.cantidad[j] = 0;
		}

		for (size_t j = 0; j < 25; j++){
			jugs[i].pared[j / 5][j % 5] = VACIO;
		}

		jugs[i].piso = 0;
		jugs[i].primerTurno = false;
	}
}
