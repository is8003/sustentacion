#include <stdio.h>
#include <stdlib.h>
#include "proveedor.h"

static void volcarFila(unsigned orig[], unsigned dest[]);
static bool colorValido(char colChar, const unsigned fila[], Color *azColPtr);
static void imprimirFuente(const unsigned fila[]);
Color escogerColor(const unsigned azs[][5], unsigned fuente);
static void imprimirProveedor(const Proveedor *partPtr);
static bool fuenteValida(const unsigned azs[][5], unsigned *fuPtr);
static unsigned escogerFuente(const Proveedor *partPtr);

static void volcarFila(unsigned orig[], unsigned dest[]){
	for(size_t i = 0; i < 5; i++){
		dest[i] += orig[i];
		orig[i] = 0;
	}
}

static bool colorValido(char colChar, const unsigned fila[], Color *azColPtr){
	switch (colChar){
		case 'A':
		case 'a':
			*azColPtr = AZUL;
			break;
		case 'R':
		case 'r':
			*azColPtr = ROJO;
			break;
		case 'M':
		case 'm':
			*azColPtr = MORADO;
			break;
		case 'N':
		case 'n':
			*azColPtr = NEGRO;
			break;
		case 'B':
		case 'b':
			*azColPtr = BLANCO;
			break;
		default:
			printf("%c no es una entrada válida.\n", colChar);
			puts("Por favor intenta nuevamente.");
			return false;
	}

	if(fila[*azColPtr - 1] < 1){
		printf("No se encuentran azulejos del color: %c\n", colChar);
		puts("Por favor intenta nuevamente.");
		return false;
	}

	return true;
}

static void imprimirFuente(const unsigned fila[]){
	for (size_t i = 0; i < 5; i++){
		unsigned azCan = fila[i];

		while(azCan > 0){
			imprimirAzulejo(i + 1);
			azCan--;
		}
	}
}

Color escogerColor(const unsigned azs[][5], unsigned fuente){
	char colChar;
	Color azCol;

	do {
		imprimirFuente(azs[fuente]);

		puts("");
		if (fuente != 2){
			printf("Escoge un color de mostrador %u: ", fuente - 2);
		}
		else {
			printf("%s","Escoge un color de centro de mesa: ");
		}

		scanf(" %c", &colChar);
	} while (!colorValido(colChar, azs[fuente], &azCol));

	return azCol;
}

static void imprimirProveedor(const Proveedor *partPtr){
	for (size_t i = 3; i < FIL_AZU; i++){
		printf("%zd. Mostrador %zd: ", i - 2, i - 2);
		imprimirFuente(partPtr->azulejos[i]);
		puts("");
	}

	printf("%d. Centro de mesa: ", FIL_AZU - 2);
	imprimirFuente(partPtr->azulejos[2]);
	printf("\n   Marcador: %s\n", (partPtr->marcadorTurno) ? "Sí" : "No");
}

static bool fuenteValida(const unsigned azs[][5], unsigned *fuPtr){
	if (*fuPtr < 1 || *fuPtr > CANT_MOST + 1){
		printf("\nFuente no válida, por favor ingrese un valor entre 1"\
				"y %u.\n\n", CANT_MOST + 1);
		return false;
	}

	*fuPtr = (*fuPtr == CANT_MOST + 1) ? 2 : *fuPtr + 2;

	if (sumarFila(azs[*fuPtr]) < 1){
		puts("\nFuente vacía, por favor escoja otra fuente.\n");
		return false;
	}

	return true;
}

static unsigned escogerFuente(const Proveedor *partPtr){
	unsigned fuente;

	do{
		imprimirProveedor(partPtr);

		printf("%s", "Selecciona fuente: "); 
		scanf("%u", &fuente);
	}while(!fuenteValida(partPtr->azulejos, &fuente));

	return fuente;
}

bool escogerAzulejos(Proveedor * partPtr, Color * azColPtr, unsigned * azCanPtr){
	bool marcIni = false;
	unsigned selFuente = escogerFuente(partPtr);

	if (selFuente == 2 && partPtr->marcadorTurno){
		marcIni = true;
		partPtr->marcadorTurno = false;
	}

	*azColPtr = escogerColor((const unsigned (*)[])partPtr->azulejos, 
			selFuente);

	*azCanPtr = partPtr->azulejos[selFuente][*azColPtr - 1]; 
	partPtr->azulejos[selFuente][*azColPtr - 1] = 0; 

	if (selFuente >= 3 && selFuente <= FIL_AZU){
		volcarFila(partPtr->azulejos[selFuente],partPtr->azulejos[2]);
	}

	return marcIni;
}

bool proveedorVacio(const unsigned azs[][5]){
	for (size_t i = 2; i < FIL_AZU; i++){
		if (sumarFila(azs[i]) > 0){
			return false;
		}
	}

	return true;
}

void surtirMostradores(Proveedor * partPtr){
	unsigned cantBolsa = sumarFila(partPtr->azulejos[0]);

	if (cantBolsa < 20){
		volcarFila(partPtr->azulejos[1],partPtr->azulejos[0]);
		cantBolsa = sumarFila(partPtr->azulejos[0]);

		if (cantBolsa < 20){
			puts("\nAdvertencia: no hay azulejos suficientes en "\
					"bolsa para una nueva ronda");
			puts("Surtiendo mostradores incompletos...");
		}
	}

	for (size_t i = 3; i < FIL_AZU; i++){
		for (size_t j = 0; j < 4 && cantBolsa > 0; j++){
			unsigned jCo;

			do {
				jCo = rand() % 5;
			} while(partPtr->azulejos[0][jCo] == 0);

			partPtr->azulejos[i][jCo]++;
			partPtr->azulejos[0][jCo]--;
			cantBolsa--;
		}
	}

	partPtr->marcadorTurno = true;
}

void iniciarPartida(Proveedor * partPtr){
	for (size_t i = 0; i < FIL_AZU * 5; i++){
		if (i / 5 != 0){
			partPtr->azulejos[i / 5][i % 5] = 0;
		}
		else {
			partPtr->azulejos[i / 5][i % 5] = 20;
		}
	}

	partPtr->marcadorTurno = true;
}
