#include "minunit/minunit.h"
#include "../jugador.c"

const Color MOSAICO[5][5] = {
	{ AZUL, ROJO, MORADO, NEGRO, BLANCO },
	{ BLANCO, AZUL, ROJO, MORADO, NEGRO },
	{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
	{ MORADO, NEGRO, BLANCO, AZUL, ROJO },
	{ ROJO, MORADO, NEGRO, BLANCO, AZUL }
};

MU_TEST(test_mosaico){
	puts("\nComprobando distribución de mosaico:");
	for (size_t i = 0; i < 5; i++){
		for (size_t j = 1; j < 5; j++){
			mu_assert_int_eq(MOSAICO[0][i],MOSAICO[j][(i + j) % 5]);
		}
	}
}

MU_TEST(test_calcularPenalizaciones){
	unsigned piso[8] = { 0, 1, 2, 3, 4, 5, 6, 7};
	unsigned res[8] = { 0, 1, 2, 4, 6, 8, 11, 14};

	for (size_t i = 0; i < 8; i++){
		mu_assert_int_eq(res[i], calcularPenalizaciones(&piso[i]));
		mu_assert_int_eq(0, piso[i]);
	}
}

MU_TEST(test_contarAdyacentes){
	const Color pared[5][5] = {
		{ VACIO, VACIO, MORADO, VACIO, VACIO },
		{ VACIO, VACIO, ROJO, VACIO, VACIO },
		{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
		{ VACIO, VACIO, BLANCO, VACIO, VACIO },
		{ VACIO, VACIO, NEGRO, VACIO, VACIO }
	};

	mu_assert_int_eq(2,contarAdyacentes(pared, 2, 2, 1, 0));
	mu_assert_int_eq(2,contarAdyacentes(pared, 2, 2, -1, 0));
	mu_assert_int_eq(2,contarAdyacentes(pared, 2, 2, 0, 1));
	mu_assert_int_eq(2,contarAdyacentes(pared, 2, 2, 0, -1));

	mu_assert_int_eq(0,contarAdyacentes(pared, 1, 2, 1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 1, 2, -1, 0));
	mu_assert_int_eq(3,contarAdyacentes(pared, 1, 2, 0, 1));
	mu_assert_int_eq(1,contarAdyacentes(pared, 1, 2, 0, -1));

	mu_assert_int_eq(0,contarAdyacentes(pared, 3, 2, 1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 3, 2, -1, 0));
	mu_assert_int_eq(1,contarAdyacentes(pared, 3, 2, 0, 1));
	mu_assert_int_eq(3,contarAdyacentes(pared, 3, 2, 0, -1));

	mu_assert_int_eq(0,contarAdyacentes(pared, 0, 2, 1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 0, 2, -1, 0));
	mu_assert_int_eq(4,contarAdyacentes(pared, 0, 2, 0, 1));
	mu_assert_int_eq(0,contarAdyacentes(pared, 0, 2, 0, -1));

	mu_assert_int_eq(0,contarAdyacentes(pared, 4, 2, 1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 4, 2, -1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 4, 2, 0, 1));
	mu_assert_int_eq(4,contarAdyacentes(pared, 4, 2, 0, -1));

	mu_assert_int_eq(4,contarAdyacentes(pared, 2, 0, 1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 0, -1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 0, 0, 1));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 0, 0, -1));

	mu_assert_int_eq(3,contarAdyacentes(pared, 2, 1, 1, 0));
	mu_assert_int_eq(1,contarAdyacentes(pared, 2, 1, -1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 1, 0, 1));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 1, 0, -1));

	mu_assert_int_eq(1,contarAdyacentes(pared, 2, 3, 1, 0));
	mu_assert_int_eq(3,contarAdyacentes(pared, 2, 3, -1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 3, 0, 1));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 3, 0, -1));

	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 4, 1, 0));
	mu_assert_int_eq(4,contarAdyacentes(pared, 2, 4, -1, 0));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 4, 0, 1));
	mu_assert_int_eq(0,contarAdyacentes(pared, 2, 4, 0, -1));
}

MU_TEST(test_buscarColor){
	const Color data[2][5] = {
		{ AZUL, ROJO, MORADO, NEGRO, BLANCO},
		{ VACIO, VACIO, VACIO, VACIO, VACIO}
	};

	for (size_t i = 0; i < 5; i++){
		mu_assert_int_eq(i,buscarColor(data[0],data[0][i]));
		mu_assert_int_eq(-1,buscarColor(data[1],data[0][i]));
	}

}

MU_TEST(test_impFilaPared){
	const Color data[2][5] = {
		{ VACIO, ROJO, VACIO, NEGRO, VACIO},
		{ MORADO, VACIO, BLANCO, VACIO, ROJO}
	};

	puts("\nPrueba impresión fila 0 de pared:");
	impFilaPared(data[0],0);
	puts("\nPrueba impresión fila 3 de pared:");
	impFilaPared(data[1],3);
	puts("");
}

MU_TEST(test_impFilaPatron){
	const Color azCol[5] = { AZUL, ROJO, VACIO, NEGRO, BLANCO};
	const unsigned azCan[5] = { 1, 1, 0, 4, 2};

	puts("\nPrueba impresión filas de patrones:");
	for (size_t i = 0; i < 5; i++){
		impFilaPatron(i, azCol[i], azCan[i]);
		puts("");
	}
}

MU_TEST(test_escogerPatron){
	const Color pared[5][5] = {
		{ VACIO, VACIO, MORADO, VACIO, VACIO },
		{ VACIO, VACIO, ROJO, VACIO, VACIO },
		{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
		{ VACIO, VACIO, VACIO, VACIO, VACIO },
		{ VACIO, VACIO, NEGRO, VACIO, VACIO }
	};
	const Patrones patr = {
		{AZUL, VACIO, MORADO, VACIO, BLANCO},
		{1, 0, 2, 0, 3}
	};

	mu_assert_int_eq(3, escogerPatron(ROJO, &patr, pared));
}

MU_TEST(test_escogerPatron_noValido){
	const Color pared[5][5] = {
		{ VACIO, VACIO, MORADO, VACIO, VACIO },
		{ VACIO, VACIO, ROJO, VACIO, VACIO },
		{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
		{ VACIO, VACIO, VACIO, VACIO, ROJO },
		{ VACIO, VACIO, NEGRO, VACIO, VACIO }
	};
	const Patrones patr = {
		{AZUL, VACIO, MORADO, VACIO, BLANCO},
		{1, 0, 2, 0, 3}
	};

	mu_assert_int_eq(-2, escogerPatron(ROJO, &patr, pared));
}

MU_TEST(test_calcularPuntajeFinal){
	Jugador jugs[4] = {
		{
			"JugadorA",
			0,
			{
				{ 0,  0,  0,  0,  0},
				{ 0,  0,  0,  0,  0}
			},
			{
				{ VACIO, VACIO, MORADO, VACIO, VACIO },
				{ VACIO, VACIO, ROJO, VACIO, VACIO },
				{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
				{ VACIO, VACIO, BLANCO, VACIO, VACIO },
				{ VACIO, VACIO, NEGRO, VACIO, VACIO }
			},
			0,
			false
		},
		{
			"JugadorB",
			0,
			{
				{ 0,  0,  0,  0,  0},
				{ 0,  0,  0,  0,  0}
			},
			{
				{ AZUL, VACIO, MORADO, NEGRO, VACIO },
				{ VACIO, AZUL, ROJO, VACIO, NEGRO },
				{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
				{ VACIO, NEGRO, VACIO, AZUL, VACIO },
				{ VACIO, VACIO, NEGRO, VACIO, AZUL }
			},
			0,
			false
		},
	};

	calcularPuntajeFinal(jugs);

	puts("\nPruebas puntaje final:");
	mu_assert_int_eq(9, jugs[0].puntos);
	mu_assert_int_eq(20, jugs[1].puntos);
}

MU_TEST(test_calcularPuntaje){
	Jugador jugs[4] = {
		{
			"JugadorA",
			0,
			{
				{ ROJO, VACIO, VACIO, VACIO, VACIO },
				{ 1,  0,  0,  0,  0}
			},
			{
				{ VACIO, VACIO, VACIO, VACIO, VACIO },
				{ VACIO, VACIO, VACIO, VACIO, VACIO },
				{ VACIO, VACIO, VACIO, VACIO, VACIO },
				{ VACIO, VACIO, VACIO, VACIO, VACIO },
				{ VACIO, VACIO, VACIO, VACIO, VACIO }
			},
			0,
			false
		},
		{
			"JugadorB",
			0,
			{
				{ VACIO, VACIO, AZUL, VACIO, VACIO },
				{ 0,  0,  3,  0,  0}
			},
			{
				{ VACIO, VACIO, MORADO, VACIO, VACIO },
				{ VACIO, VACIO, ROJO, VACIO, VACIO },
				{ NEGRO, BLANCO, VACIO, ROJO, MORADO },
				{ VACIO, VACIO, BLANCO, VACIO, VACIO },
				{ VACIO, VACIO, NEGRO, VACIO, VACIO }
			},
			0,
			false
		},
	};

	Color basu[5] = {0};

	calcularPuntaje(jugs, basu);

	puts("\nPruebas puntaje:");
	mu_assert_int_eq(1, jugs[0].puntos);
	mu_assert_int_eq(2, basu[0]);
	mu_assert_int_eq(10, jugs[1].puntos);
	mu_assert_int_eq(0, basu[1]);
}

MU_TEST(test_ubicarAzulejos){
	const Color azCol = ROJO;
	unsigned azCan = 4;
	unsigned piso = 0;
	Color pared[5][5] = {
		{ VACIO, VACIO, MORADO, VACIO, VACIO },
		{ VACIO, VACIO, VACIO, VACIO, VACIO },
		{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
		{ VACIO, VACIO, VACIO, VACIO, VACIO },
		{ VACIO, VACIO, NEGRO, VACIO, VACIO }
	};
	Patrones patr = {
		{AZUL, VACIO, MORADO, VACIO, BLANCO},
		{1, 0, 2, 0, 3}
	};

	mu_assert_int_eq(2, ubicarAzulejos(azCol, azCan, &patr, pared, &piso));
	mu_assert_int_eq(2, piso);
}

MU_TEST(test_ubicarAzulejos_noValido){
	const Color azCol = ROJO;
	unsigned azCan = 4;
	unsigned piso = 0;
	Color pared[5][5] = {
		{ VACIO, VACIO, MORADO, VACIO, VACIO },
		{ VACIO, VACIO, ROJO, VACIO, VACIO },
		{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
		{ VACIO, VACIO, VACIO, VACIO, ROJO },
		{ VACIO, VACIO, NEGRO, VACIO, VACIO }
	};
	Patrones patr = {
		{AZUL, VACIO, MORADO, VACIO, BLANCO},
		{1, 0, 2, 0, 3}
	};

	mu_assert_int_eq(4, ubicarAzulejos(azCol, azCan, &patr, pared, &piso));
	mu_assert_int_eq(4, piso);
}

MU_TEST(test_imprimirTablero){
	Jugador jug = {
		"JugadorA",
		28,
		{
			{ AZUL,  VACIO,  ROJO,  VACIO,  VACIO},
			{ 1,  0,  2,  0,  0}
		},
		{
			{ VACIO, VACIO, MORADO, VACIO, VACIO },
			{ VACIO, VACIO, ROJO, VACIO, VACIO },
			{ NEGRO, BLANCO, AZUL, VACIO, MORADO },
			{ VACIO, VACIO, BLANCO, VACIO, VACIO },
			{ VACIO, VACIO, NEGRO, VACIO, VACIO }
		},
		1,
		false
	};

	imprimirTablero(&jug);
}

MU_TEST(test_escogerIniciadorRonda){
	Jugador jugs[4] = {
		{
			"JugadorA",
			0,
			{
				{ 0,  0,  0,  0,  0},
				{ 0,  0,  0,  0,  0}
			},
			{
				{ VACIO, VACIO, MORADO, VACIO, VACIO },
				{ VACIO, VACIO, ROJO, VACIO, VACIO },
				{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
				{ VACIO, VACIO, BLANCO, VACIO, VACIO },
				{ VACIO, VACIO, NEGRO, VACIO, VACIO }
			},
			0,
			false
		},
		{
			"JugadorB",
			0,
			{
				{ 0,  0,  0,  0,  0},
				{ 0,  0,  0,  0,  0}
			},
			{
				{ AZUL, VACIO, MORADO, NEGRO, VACIO },
				{ VACIO, AZUL, ROJO, VACIO, NEGRO },
				{ NEGRO, BLANCO, AZUL, ROJO, VACIO },
				{ VACIO, NEGRO, VACIO, AZUL, VACIO },
				{ VACIO, VACIO, NEGRO, VACIO, AZUL }
			},
			0,
			true
		},
	};


	puts("\nPruebas iniciador de ronda:");
	mu_assert_int_eq(1, escogerIniciadorRonda(jugs));
}

MU_TEST(test_iniciarJugadores){
	Jugador jugs[4];

	iniciarJugadores(jugs);
	mu_assert_string_eq("JugadorA", jugs[0].nombre);
	mu_assert_string_eq("JugadorB", jugs[1].nombre);
	mu_assert_int_eq(0, jugs[0].puntos);
	mu_assert_int_eq(0, jugs[1].puntos);

	for (size_t i = 0; i < 5; i++){
		mu_assert_int_eq(VACIO, jugs[0].lineas.colorAzulejo[i]);
		mu_assert_int_eq(VACIO, jugs[1].lineas.colorAzulejo[i]);
		mu_assert_int_eq(0, jugs[0].lineas.cantidad[i]);
		mu_assert_int_eq(0, jugs[1].lineas.cantidad[i]);
	}

	mu_assert_int_eq(0, jugs[0].piso);
	mu_assert_int_eq(0, jugs[1].piso);

	mu_check(!jugs[0].primerTurno);
	mu_check(!jugs[1].primerTurno);
}

MU_TEST_SUITE(test_jugador_suite){
	MU_RUN_TEST(test_mosaico);
	MU_RUN_TEST(test_calcularPenalizaciones);
	MU_RUN_TEST(test_contarAdyacentes);
	MU_RUN_TEST(test_buscarColor);
	MU_RUN_TEST(test_impFilaPared);
	MU_RUN_TEST(test_impFilaPatron);
	MU_RUN_TEST(test_escogerPatron_noValido);
	MU_RUN_TEST(test_calcularPuntajeFinal);
	MU_RUN_TEST(test_calcularPuntaje);
	MU_RUN_TEST(test_ubicarAzulejos_noValido);
	MU_RUN_TEST(test_imprimirTablero);
	MU_RUN_TEST(test_escogerIniciadorRonda);
}

MU_TEST_SUITE(test_jugador_suite_interactivo){
	MU_RUN_TEST(test_escogerPatron);
	MU_RUN_TEST(test_ubicarAzulejos);
	MU_RUN_TEST(test_iniciarJugadores);
}

int main(void){
	MU_RUN_SUITE(test_jugador_suite);
	MU_RUN_SUITE(test_jugador_suite_interactivo);
	MU_REPORT();
	return MU_EXIT_CODE;
}
