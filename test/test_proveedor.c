#include "minunit/minunit.h"
#include "../proveedor.c"

MU_TEST(test_volcarFila) {
	unsigned n = 5;
	unsigned origen[] = {1, 2, 3, 4, 5};
	unsigned destino[] = {1, 4, 3, 2, 1};
	unsigned eleSum[n];

	for (unsigned i = 0; i < n; i++){
		eleSum[i] = origen[i] + destino[i];
	}

	volcarFila(origen, destino);

	for (unsigned i = 0; i < n; i++){
		mu_assert_int_eq(eleSum[i], destino[i]);
	}

	for (unsigned i = 0; i < n; i++){
		mu_assert_int_eq(0, origen[i]);
	}

}

MU_TEST(test_colorValido_encontrado){
	char colChar[10] = {'A', 'a', 'R', 'r', 'M', 'm', 'N', 'n', 'B', 'b'};
	Color colNum[10] = {AZUL, AZUL, ROJO, ROJO, MORADO, MORADO, NEGRO, 
		NEGRO, BLANCO, BLANCO};
	unsigned fila[] = {1, 2, 3, 4, 5};
	unsigned azCol;

	for (size_t i = 0; i < 10; i++){
		mu_check(colorValido(colChar[i], fila, &azCol));
		mu_assert_int_eq(colNum[i],azCol);
	}
}

MU_TEST(test_colorValido_vacio){
	char colChar[10] = {'A', 'a', 'R', 'r', 'M', 'm', 'N', 'n', 'B', 'b'};
	Color colNum[10] = {AZUL, AZUL, ROJO, ROJO, MORADO, MORADO, NEGRO, 
		NEGRO, BLANCO, BLANCO};
	unsigned fila[5] = {0};
	unsigned azCol = 100;

	for (size_t i = 0; i < 10; i++){
		mu_check(!colorValido(colChar[i], fila, &azCol));
		mu_assert_int_eq(colNum[i],azCol);
	}
}

MU_TEST(test_colorValido_entradaMal){
	char colChar[3] = {'x', 'Y', 'z'};
	unsigned fila[] = {1, 2, 3, 4, 5};
	unsigned azCol = 100;

	for (size_t i = 0; i < 3; i++){
		mu_check(!colorValido(colChar[i], fila, &azCol));
		mu_assert_int_eq(100,azCol);
	}
}

MU_TEST(test_imprimirFuente){
	const unsigned azules[] = {20, 0, 0, 0, 0};
	const unsigned rojos[] = {0, 20, 0, 0, 0};
	const unsigned morados[] = {0, 0, 20, 0, 0};
	const unsigned negros[] = {0, 0, 0, 20, 0};
	const unsigned blancos[] = {0, 0, 0, 0, 20};
	const unsigned fuente[] = {0, 2, 0, 0, 2};
	const unsigned vacio[5] = {0};

	puts("\nImprimiendo por color: ");
	imprimirFuente(azules);
	puts("");
	imprimirFuente(rojos);
	puts("");
	imprimirFuente(morados);
	puts("");
	imprimirFuente(negros);
	puts("");
	imprimirFuente(blancos);
	puts("\nImprimiendo fuente: ");
	imprimirFuente(fuente);
	puts("\nImprimiendo vacio: ");
	imprimirFuente(vacio);
}

MU_TEST(test_escogerColor){
	unsigned azs[FIL_AZU][5];

	for (size_t i = 0; i < FIL_AZU * 5; i++){
		azs[i / 5][i % 5] = ((i / 5) % 5 == i % 5) ? 1 : 0;
	}

	for (size_t i = 3; i <= 7; i++){ // Mínimo de mostradores
		printf("\nescogerColor - prueba %zd:\n", i - 3);
		mu_assert_int_eq((i % 5) + 1,
				escogerColor((const unsigned (*)[])azs, i));
	}
}

MU_TEST(test_imprimirProveedor){
	Proveedor part;

	for (size_t i = 0; i < FIL_AZU * 5; i++){
		part.azulejos[i / 5][i % 5] = ((i / 5) % 5 == i % 5) ? 1 : 0;
	}
	part.marcadorTurno = false;

	puts("\nImpresión de proveedor 1:");
	imprimirProveedor(&part);

	for (size_t i = 15; i < FIL_AZU * 5; i++){
		part.azulejos[i / 5][i % 5] += ((i / 5) % 5 == i % 5) ? 3 : 0;
	}
	part.azulejos[2][2] = 0;
	part.marcadorTurno = true;

	puts("\nImpresión de proveedor 2:");
	imprimirProveedor(&part);
}

MU_TEST(test_fuenteValida){
	unsigned azs[FIL_AZU][5];

	for (size_t i = 0; i < FIL_AZU * 5; i++){
		azs[i / 5][i % 5] = ((i / 5) % 2 && (i / 5) % 5 == i % 5) ? 1 : 0;
	}

	for (size_t i = 0; i <= CANT_MOST + 1; i++){
		printf("\nfuenteValida - prueba %zd:\n", i);
		unsigned fue = i;
		if (i % 2){
			mu_check(fuenteValida((const unsigned (*)[])azs, &fue));
			mu_assert_int_eq(i + 2, fue);
		}
		else{
			mu_check(!fuenteValida((const unsigned (*)[])azs, &fue));
		}
	}
}

MU_TEST(test_escogerFuente){
	Proveedor part = {
		{
			{16, 16, 16, 14, 16},
			{0},
			{0, 0, 0, 2, 0},
			{4, 0, 0, 0, 0},
			{0, 4, 0, 0, 0},
			{0, 0, 4, 0, 0},
			{0, 0, 0, 4, 0},
			{0, 0, 0, 0, 0},
		},
		false
	};

	puts("\nPrueba escogerFuente() - mostrador");
	mu_assert_int_eq(3, escogerFuente(&part));

	puts("\nPrueba escogerFuente() - centro de mesa");
	mu_assert_int_eq(2, escogerFuente(&part));
}

MU_TEST(test_escogerAzulejos_limpia){
	Color azCol;
	unsigned azCan;
	Proveedor part = {
		{
			{16, 16, 16, 16, 16},
			{0},
			{0},
			{4, 0, 0, 0, 0},
			{0, 4, 0, 0, 0},
			{0, 0, 4, 0, 0},
			{0, 0, 0, 4, 0},
			{0, 0, 0, 0, 4},
		},
		true
	};

	puts("\nPrueba escogerAzulejos() sin sobrante");
	mu_check(!escogerAzulejos(&part, &azCol, &azCan));
	mu_assert_int_eq(AZUL,azCol);
	mu_assert_int_eq(4,azCan);
	mu_assert_int_eq(0,sumarFila(part.azulejos[2]));
	mu_assert_int_eq(0,sumarFila(part.azulejos[3]));
}

MU_TEST(test_escogerAzulejos_sucio){
	Color azCol;
	unsigned azCan;
	Proveedor part = {
		{
			{14, 16, 16, 18, 16},
			{0},
			{0},
			{4, 0, 0, 0, 0},
			{0, 4, 0, 0, 0},
			{0, 0, 4, 0, 0},
			{2, 0, 0, 2, 0},
			{0, 0, 0, 0, 4},
		},
		true
	};

	puts("\nPrueba escogerAzulejos() con sobrante");
	mu_check(!escogerAzulejos(&part, &azCol, &azCan));
	mu_assert_int_eq(NEGRO,azCol);
	mu_assert_int_eq(2,azCan);
	mu_assert_int_eq(2,sumarFila(part.azulejos[2]));
	mu_assert_int_eq(0,sumarFila(part.azulejos[6]));
}

MU_TEST(test_escogerAzulejos_centro){
	Color azCol;
	unsigned azCan;
	Proveedor part = {
		{
			{16, 16, 16, 16, 16},
			{0},
			{0, 2, 1, 0, 3},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
		},
		true
	};

	puts("\nPrueba escogerAzulejos() centro de mesa");
	mu_check(escogerAzulejos(&part, &azCol, &azCan));
	mu_assert_int_eq(MORADO,azCol);
	mu_assert_int_eq(1,azCan);
	mu_assert_int_eq(5,sumarFila(part.azulejos[2]));
}

MU_TEST(test_proveedorVacio){
	Proveedor part = {
		{
			{20, 20, 20, 20, 20}
		},
		false
	};

	puts("\nPrueba proveedorVacio() - vacio");
	mu_check(proveedorVacio((const unsigned (*)[])part.azulejos));

	part.azulejos[2][4] = 1;
	puts("\nPrueba proveedorVacio() - no vacio");
	mu_check(!proveedorVacio((const unsigned (*)[])part.azulejos));
}

MU_TEST(test_surtirMostradores_inicio){
	Proveedor part = {
		{
			{20, 20, 20, 20, 20}
		},
		false
	};

	srand(time(NULL));

	puts("\nPrueba surtirMostradores() - inicio");
	surtirMostradores(&part);
	for (size_t i = 3; i < FIL_AZU; i++){
		mu_assert_int_eq(4, sumarFila(part.azulejos[i]));
	}
	mu_assert_int_eq(100 - CANT_MOST * 4, sumarFila(part.azulejos[0]));
	mu_check(part.marcadorTurno);
}

MU_TEST(test_surtirMostradores_fin){
	Proveedor part = {
		{
			{2, 2, 2, 2, 2}
		},
		false
	};

	unsigned azulejosServidos = 0;

	srand(time(NULL));

	puts("\nPrueba surtirMostradores() - fin");
	surtirMostradores(&part);
	for (size_t i = 3; i < FIL_AZU; i++){
		azulejosServidos += sumarFila(part.azulejos[i]);
	}
	mu_assert_int_eq(10, azulejosServidos);
	mu_assert_int_eq(0, sumarFila(part.azulejos[0]));
	mu_check(part.marcadorTurno);
}

MU_TEST(test_iniciarPartida){
	Proveedor part;

	puts("\nPrueba iniciarPartida()");
	iniciarPartida(&part);
	mu_assert_int_eq(100, sumarFila(part.azulejos[0]));
	for (size_t i = 1; i < FIL_AZU; i++){
		mu_assert_int_eq(0, sumarFila(part.azulejos[i]));
	}
	mu_check(part.marcadorTurno);
}

MU_TEST_SUITE(test_proveedor_suite) {
	MU_RUN_TEST(test_volcarFila);
	MU_RUN_TEST(test_colorValido_encontrado);
	MU_RUN_TEST(test_colorValido_vacio);
	MU_RUN_TEST(test_colorValido_entradaMal);
	MU_RUN_TEST(test_imprimirFuente);
	MU_RUN_TEST(test_imprimirProveedor);
	MU_RUN_TEST(test_fuenteValida);
	MU_RUN_TEST(test_proveedorVacio);
	MU_RUN_TEST(test_surtirMostradores_inicio);
	MU_RUN_TEST(test_surtirMostradores_fin);
	MU_RUN_TEST(test_iniciarPartida);
}

MU_TEST_SUITE(test_proveedor_interactivo_suite) {
	MU_RUN_TEST(test_escogerColor);
	MU_RUN_TEST(test_escogerFuente);
	MU_RUN_TEST(test_escogerAzulejos_limpia);
	MU_RUN_TEST(test_escogerAzulejos_sucio);
	MU_RUN_TEST(test_escogerAzulejos_centro);
}

int main(void) {
	puts("\n### Pruebas por lote ###");
	MU_RUN_SUITE(test_proveedor_suite);

	puts("\n\n### Pruebas interactivas ###");
	MU_RUN_SUITE(test_proveedor_interactivo_suite);
	MU_REPORT();

	return MU_EXIT_CODE;
}
