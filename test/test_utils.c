#include "minunit/minunit.h"
#include "../utils.c"


MU_TEST(test_sumaFila) {
	unsigned vacio[] = {0, 0, 0, 0, 0};
	unsigned medio[] = {1, 0, 1, 0, 1};
	unsigned lleno[] = {1, 2, 3, 4, 5};

	mu_assert_int_eq(0, sumarFila(vacio));
	mu_assert_int_eq(3, sumarFila(medio));
	mu_assert_int_eq(15, sumarFila(lleno));
}

MU_TEST(test_imprimirAzulejo) {
	for (int azCol = -5; azCol < 6; azCol++){
		mu_assert_int_eq(EXIT_SUCCESS, imprimirAzulejo(azCol));
	}
	mu_assert_int_eq(EXIT_FAILURE, imprimirAzulejo(-6));
	mu_assert_int_eq(EXIT_FAILURE, imprimirAzulejo(-10));
	mu_assert_int_eq(EXIT_FAILURE, imprimirAzulejo(6));
	mu_assert_int_eq(EXIT_FAILURE, imprimirAzulejo(40));
}

MU_TEST_SUITE(test_sumaFila_suite) {
	MU_RUN_TEST(test_sumaFila);
	MU_RUN_TEST(test_imprimirAzulejo);
}

int main(void) {
	MU_RUN_SUITE(test_sumaFila_suite);
	MU_REPORT();
	return MU_EXIT_CODE;
}
